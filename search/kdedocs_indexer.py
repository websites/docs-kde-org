#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2015-2016  Luigi Toscano <luigi.toscano@tiscali.it>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License or (at your option) version 3 or any later version
# accepted by the membership of KDE e.V. (or its successor approved
# by the membership of KDE e.V.), which shall act as a proxy
# defined in Section 14 of version 3 of the license.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import hashlib
import logging
import os
import re
import sys
import time
from collections import namedtuple

from bs4 import BeautifulSoup
import unicodedata
import xapian

SAMPLESIZE = 512

IDX_VAL_LASTMOD = 0
IDX_VAL_MD5 = 1
IDX_VAL_SIZE = 2

STEMMER_MAP = {
    'da': 'danish',
    'de': 'german',
    # de alternative: german2
    'en': 'english',
    'en_GB': 'english',
    # en* alternatives: lovins porter
    'es': 'spanish',
    'fi': 'finnish',
    'fr': 'french',
    'hu': 'hungarian',
    'it': 'italian',
    'nb': 'norwegian',
    'nn': 'norwegian',
    'nl': 'dutch',
    # nl alternative: kraaij_pohlmann
    'pt': 'portuguese',
    'pt_BR': 'portuguese',
    'ro': 'romanian',
    'ru': 'russian',
    'sv': 'swedish',
    'tr': 'turkish',
}

ALL_BRANCHES = ['stable5', 'trunk5']

LOGGER = logging.getLogger(__name__)

DocDetails = namedtuple('DocDetails', ['content', 'md5', 'path', 'title',
                                       'mtime', 'size', 'description',
                                       'keywords', 'application'])


def get_normalized_branch(branch):
    if branch.startswith('stable'):
        return 'stable'
    elif branch.startswith('trunk'):
        return 'trunk'
    else:
        return ''


def get_stemmer_name(lang):
    """ Returns the name of the stemmer for the specified language code. """
    return STEMMER_MAP.get(lang, 'none')


def opendb(dbfile):
    database = xapian.WritableDatabase(dbfile, xapian.DB_CREATE_OR_OPEN)
    indexer = xapian.TermGenerator()
    return (database, indexer)


def get_doc_details(full_path, file_path_rel):
    try:
        html_file = open(full_path, 'r')
        html_parsed = BeautifulSoup(html_file, "lxml")
    except Exception as e:
        LOGGER.error("Exception parsing %s: %s" % (full_path, str(e)),
                     exc_info=True)
        return None

    hashf = hashlib.md5()
    hashf.update(str(html_parsed))
    docmd5 = hashf.digest()

    dumptxt = ' '.join(html_parsed.findAll(text=True))
    doctxt = unicodedata.normalize('NFKC', dumptxt)
    doctitle = html_parsed.title.string
    docmtime = int(os.path.getmtime(full_path))
    docsize = os.path.getsize(full_path)
    appname = os.path.basename(os.path.dirname(full_path))

    # description
    descriptions = html_parsed.findAll(attrs={"name": "description"})
    if descriptions:
        docdescription = descriptions[-1]['content']
    else:
        docdescription = doctxt
    # keywords:
    keywords = html_parsed.findAll(attrs={"name": "keywords"})
    dockeywords = []
    if keywords:
        dockeywords = re.split('[,\s]+', keywords[-1]['content'])

    doc_details = DocDetails(doctxt, docmd5, file_path_rel, doctitle, docmtime,
                             docsize, docdescription, dockeywords, appname)
    return doc_details


def do_index_file(file_path, file_path_rel, xdb, xidx, language, branch,
                  normalized_branch):
    """ Check a single file and determine whether and how it should be
        indexed.
    """
    doc_urlterm = 'U%s' % (file_path_rel)
    # check if a document with the same U<path> tag already exists
    # in the database, and the respective timestamp (mtime)
    currdoc_mtime = int(os.path.getmtime(file_path))
    postlist = xdb.postlist(doc_urlterm)
    try:
        existing_doc_iter = next(postlist)
        existing_doc_id = existing_doc_iter.docid
        existing_doc = xdb.get_document(existing_doc_id)
        existing_doc_lastmod = float(existing_doc.get_value(IDX_VAL_LASTMOD))
    except StopIteration:
        existing_doc_id = None
        existing_doc_lastmod = None
    LOGGER.debug('Looking for existing occurrencies of %s (timestamp %s): '
                 'found %s (timestamp: %s)' % (file_path_rel, currdoc_mtime,
                                               existing_doc_id,
                                               existing_doc_lastmod))

    if existing_doc_id and (existing_doc_lastmod >= currdoc_mtime):
        LOGGER.info('Doc %s already in the db and up-to-date' % (doc_urlterm))
        return

    doc_details = get_doc_details(file_path, file_path_rel)
    if not doc_details:
        LOGGER.error('Error accessing %s, skipping...' % (file_path))

    doc = xapian.Document()

    xidx.set_document(doc)

    sample = doc_details.description[0:SAMPLESIZE - 1]

    record_data = [
        'url=%s' % (doc_details.path),
        'sample=%s' % (sample),
        'type=text/html',
        'modtime=%d' % (doc_details.mtime),
        'size=%d' % (doc_details.size)
    ]
    if doc_details.title:
        record_data.append('caption=%s' % (
            doc_details.title[0:SAMPLESIZE - 1]))

    doc.set_data('\n'.join(record_data))

    # Index the text of the document and also various additional information
    # (title, keywords, file name)
    if doc_details.title:
        xidx.index_text(doc_details.title, 5)
    xidx.increase_termpos(100)

    if doc_details.content:
        xidx.index_text(doc_details.content)

    xidx.increase_termpos(100)
    # print "keywords: %s" % (' '.join(doc_details.keywords))
    xidx.index_text(' '.join(doc_details.keywords))

    # the file name with no extension (which is always .html)
    xidx.increase_termpos(100)
    xidx.index_text(os.path.basename(doc_details.path)[:len('.html')])

    # add boolean, add values
    doc.add_boolean_term('Ttext/html')

    file_timestamp = time.gmtime(doc_details.mtime)
    doc.add_boolean_term('D%s' % (time.strftime('%Y%m%d', file_timestamp)))
    doc.add_boolean_term('M%s' % (time.strftime('%Y%m', file_timestamp)))
    doc.add_boolean_term('Y%s' % (time.strftime('%Y', file_timestamp)))

    # URL/path
    doc.add_boolean_term('P/')
    doc.add_boolean_term(doc_urlterm)

    # extra details, specifically required for docs.kde.org search
    doc.add_boolean_term('L%s' % (language))
    doc.add_boolean_term('XBRANCH%s' % (branch))
    doc.add_boolean_term('XBRANCHGENERIC%s' % (normalized_branch))

    # used to sort by date
    doc.add_value(IDX_VAL_LASTMOD, str(doc_details.mtime))

    # used to collapse duplicate documents
    doc.add_value(IDX_VAL_MD5, str(doc_details.md5))

    # used to sort by size and for date ranges
    doc.add_value(IDX_VAL_SIZE, str(doc_details.size))

    # Finally, add the document to the database or replace it if already
    # existed and it was updated.
    if existing_doc_id:
        # this means the document exists and its timestamp shows that it
        # was updated.
        LOGGER.info("Existing doc %s was updated" % (doc_urlterm))
        xdb.replace_document(existing_doc_id, doc)
    else:
        LOGGER.info("New doc %s was added" % (doc_urlterm))
        # not existing at all, regardless of the timestamp - add it anyway
        xdb.add_document(doc)


def cleanup_unseen(xdb, seen_files):
    # check all indexed documents and remove the ones not in seen_files
    LOGGER.info('Looking for removed documents...')
    removed_count = 0
    for doc_iter in xdb.postlist(''):
        if not doc_iter.docid:
            # This should not happen, but...
            continue
        doc_ref = xdb.get_document(doc_iter.docid)
        # if no U<path> term in the document have been seen, remove it
        doc_urls = [term.term for term in doc_ref.termlist()
                    if term.term.startswith('U') and term.term in seen_files]
        if len(doc_urls) == 0:
            removed_count += 1
            LOGGER.debug('Removing %s (%s)' % (doc_iter.docid, [term.term
                                               for term in doc_ref.termlist()
                                               if term.term.startswith('U')]))
            xdb.delete_document(doc_iter.docid)
    LOGGER.info('Removed %d documents' % (removed_count))


def do_index(dbfile, startdir, languages_list):
    try:
        (xdb, xidx) = opendb(dbfile)

        # array of files encountered while searching for html files; all the
        # files not in this list after the end of the scan will be removed
        # from the index
        seen_files = set()

        # all branches, all languages, all html files
        for branch in ALL_BRANCHES:
            LOGGER.debug('Indexing branch %s' % (branch))
            # list all directories; each directory is a language
            branch_dir = os.path.join(startdir, branch)
            norm_branch = get_normalized_branch(branch)
            all_lang_dirs = [ldir for ldir in os.listdir(branch_dir)
                             if os.path.isdir(os.path.join(branch_dir,
                                                           ldir)) and
                             (not languages_list or (languages_list and
                                                     ldir in languages_list))]
            for lang in all_lang_dirs:
                LOGGER.debug('Indexing language %s' % (lang))
                xidx.set_stemmer(xapian.Stem(get_stemmer_name(lang)))
                lang_dir = os.path.join(branch_dir, lang)
                # from here, get all html files and index them with the said
                # branch/language if required
                for walked_dir, subdirs, files in os.walk(lang_dir):
                    for found_file in files:
                        if not found_file.endswith('.html'):
                            continue
                        # mark the file as seen, and call the function which
                        # whether and how the file should be updated
                        file_path = os.path.join(walked_dir, found_file)
                        file_path_rel = os.path.join('/', os.path.relpath(
                            file_path, startdir))
                        LOGGER.debug('Inspecting file %s' % (file_path_rel))
                        seen_files.add('U%s' % (file_path_rel))
                        try:
                            do_index_file(file_path, file_path_rel, xdb, xidx,
                                          lang, branch, norm_branch)
                        except Exception as e:
                            LOGGER.error("Skipping file %s, exception: %s" %
                                         (file_path_rel, str(e)),
                                         exc_info=True)
        cleanup_unseen(xdb, seen_files)
        xdb.commit()
    except Exception as e:
        LOGGER.error("Exception: %s" % str(e), exc_info=True)
        sys.exit(1)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true', default=False,
                        dest='verbose', help='verbose output')
    parser.add_argument('-l', '--languages-file', default='languages',
                        dest='languages_file', required=True,
                        help='files containing the list of languages')
    parser.add_argument('-d', '--database', default='db', dest='db',
                        required=True, metavar='DATABASE_DIR',
                        help='directory containing the Xapian database')
    parser.add_argument('-w', '--website', default='website', dest='website',
                        required=True, metavar='WEBSITE_DIR',
                        help='website directory containing the documentation')

    args = parser.parse_args()

    # configure the logging system
    logging_level = logging.INFO
    if args.verbose:
        logging_level = logging.DEBUG
    logging.basicConfig(level=logging_level,
                        format='%(asctime)s:%(levelname)s:%(name)s:'
                               '%(message)s')

    try:
        with open(args.languages_file) as lfile:
            languages_list = [lang.strip() for lang in lfile]
        if 'en' not in languages_list:
            languages_list.insert(0, 'en')
    except:
        raise ValueError('languages file "%s" not found or invalid' %
                         (args.languages_file))
        languages_list = None

    if not os.path.isdir(args.db):
        raise ValueError('%s is not a directory' % (args.db))
    if not os.path.isdir(args.website):
        raise ValueError('%s is not a directory' % (args.website))
    do_index(args.db, args.website, languages_list)


if __name__ == '__main__':
    main()
