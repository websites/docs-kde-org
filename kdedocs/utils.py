#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2015  Luigi Toscano <luigi.toscano@tiscali.it>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) version 3, or any
# later version accepted by the membership of KDE e.V. (or its
# successor approved by the membership of KDE e.V.), which shall
# act as a proxy defined in Section 6 of version 3 of the license.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see <http://www.gnu.org/licenses/>.

import codecs
import errno
import os
import shutil
from tempfile import NamedTemporaryFile


def compare_timestamp_file(timestamp, filename):
    """Compare the timestamp of the file with the specified timestamp.
    Returns a value:
        <0: if the timestamp is older than the one of the file
        =0: if the timestamps are the same.
        >0: if the timestamp is newer than the one of the file.
    """
    result = timestamp
    try:
        file_time = os.path.getmtime(filename)
        result = timestamp - file_time
    except OSError as e:
        # ignore the file, the specified timestamp is newer
        pass
    return result


def find_files_pattern(directory, name=None, pattern=None):
    found = []
    for walked_dir, subdirs, files in os.walk(directory):
        if '.svn' in subdirs:
            subdirs.remove('.svn')
        for found_file in files:
            rel_dir = os.path.relpath(walked_dir, directory)
            if pattern and found_file.find(pattern) > 0:
                found.append((rel_dir, found_file))
            elif name and found_file == name:
                found.append((rel_dir, found_file))
    return found


def file_replace_re(filename, compiled_re, replaced_string):
    tmp_file = NamedTemporaryFile(delete=False)
    with codecs.open(filename, 'r', encoding='utf-8') as sf:
        for line in sf:
            tmp_file.write(compiled_re.sub(replaced_string,
                                           line).encode('utf8'))
    tmp_file.close()
    shutil.copymode(filename, tmp_file.name)
    shutil.move(tmp_file.name, filename)


def mkdirp(path):
    """This is not needed with Python >=3.2 (exist_ok)."""
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno != errno.EEXIST or not os.path.isdir(path):
            raise
