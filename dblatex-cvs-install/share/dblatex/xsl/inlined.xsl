<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!--############################################################################
    XSLT Stylesheet DocBook -> LaTeX 
    ############################################################################ -->


<xsl:template name="inline.boldseq">
  <xsl:param name="content">
    <xsl:apply-templates/>
  </xsl:param>
  <xsl:text>\textbf{</xsl:text>
  <xsl:call-template name="string-replace">
  <xsl:with-param name="to">`</xsl:with-param>
  <xsl:with-param name="from">&#8216;</xsl:with-param>
  <xsl:with-param name="string">
    <xsl:call-template name="string-replace">
    <xsl:with-param name="to">''</xsl:with-param>
    <xsl:with-param name="from">&#8221;</xsl:with-param>
    <xsl:with-param name="string">
      <xsl:copy-of select="$content"/>
    </xsl:with-param></xsl:call-template>
  </xsl:with-param></xsl:call-template>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template name="inline.italicseq">
  <xsl:param name="content">
    <xsl:apply-templates mode="slash.hyphen"/>
  </xsl:param>
  <xsl:text>\def\-{\discretionary{}{}{}}\emph{</xsl:text>
  <xsl:copy-of select="$content"/>
  <xsl:text>}</xsl:text>
</xsl:template>

<!-- Cela prend plus de temps que la version qui suit... Bizarre.
<xsl:template name="inline.monoseq">
  <xsl:text>\texttt{</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template name="inline.italicmonoseq">
  <xsl:text>\texttt{\emph{</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>}}</xsl:text>
</xsl:template>
-->

<xsl:template name="inline.charseq">
  <xsl:param name="content">
    <xsl:apply-templates/>
  </xsl:param>
  <xsl:copy-of select="$content"/>
</xsl:template>

<xsl:template name="inline.boldcharseq">
  <xsl:param name="content">
    <xsl:apply-templates/>
  </xsl:param>
  <xsl:text>\textbf{</xsl:text>
  <xsl:copy-of select="$content"/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template name="inline.boldmonoseq">
  <xsl:param name="content">
    <xsl:apply-templates mode="slash.hyphen"/>
  </xsl:param>
  <xsl:text>\def\-{\discretionary{}{}{}}\textbf{\texttt{%% texclean(hyphenon)&#10;</xsl:text>
  <xsl:copy-of select="$content"/>
  <xsl:text>%% texclean(hyphenoff)&#10;}}</xsl:text>
</xsl:template>

<xsl:template name="inline.superscriptseq">
  <xsl:param name="content">
    <xsl:apply-templates/>
  </xsl:param>
  <xsl:text>$^{\text{</xsl:text>
  <xsl:copy-of select="$content"/>
  <xsl:text>}}$</xsl:text>
</xsl:template>

<xsl:template name="inline.subscriptseq">
  <xsl:param name="content">
    <xsl:apply-templates/>
  </xsl:param>
  <xsl:text>$_{\text{</xsl:text>
  <xsl:copy-of select="$content"/>
  <xsl:text>}}$</xsl:text>
</xsl:template>

<xsl:template name="inline.monoseq">
  <xsl:param name="content">
    <xsl:apply-templates mode="slash.hyphen"/>
  </xsl:param>
  <xsl:text>\def\-{\discretionary{}{}{}}\texttt{</xsl:text>
  <xsl:text>%% texclean(hyphenon)&#10;</xsl:text>
  <xsl:copy-of select="$content"/>
  <xsl:text>%% texclean(hyphenoff)&#10;</xsl:text>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template name="inline.italicmonoseq">
  <xsl:param name="content">
    <xsl:apply-templates/>
  </xsl:param>
  <xsl:text>\texttt{\emph{\small{</xsl:text>
  <xsl:text>%% texclean(hyphenon)&#10;</xsl:text>
  <xsl:copy-of select="$content"/>
  <xsl:text>%% texclean(hyphenoff)&#10;</xsl:text>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template name="inline.underlineseq">
  <xsl:param name="content">
    <xsl:apply-templates/>
  </xsl:param>
  <xsl:text>\underline{</xsl:text>
  <xsl:copy-of select="$content"/>
  <xsl:text>}</xsl:text>
</xsl:template>

<!-- ==================================================================== -->
<!-- some special cases -->

<xsl:template match="author|editor|othercredit|personname">
  <xsl:call-template name="normalize-scape">
    <xsl:with-param name="string">
      <xsl:call-template name="person.name"/>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="authorinitials">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="authorgroup">
  <xsl:call-template name="normalize-scape">
    <xsl:with-param name="string">
      <xsl:call-template name="person.name.list"/>
    </xsl:with-param>
  </xsl:call-template>
  <xsl:text>&#10;</xsl:text>
</xsl:template>

<!-- ==================================================================== -->

<xsl:template match="accel">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="action">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="application">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="classname">
  <xsl:call-template name="inline.monoseq"/>
</xsl:template>

<xsl:template match="exceptionname">
  <xsl:call-template name="inline.monoseq"/>
</xsl:template>

<xsl:template match="interfacename">
  <xsl:call-template name="inline.monoseq"/>
</xsl:template>

<xsl:template match="methodname">
  <xsl:call-template name="inline.monoseq"/>
</xsl:template>

<xsl:template match="command">
  <xsl:call-template name="inline.boldseq"/>
</xsl:template>

<xsl:template match="computeroutput">
  <xsl:call-template name="inline.monoseq"/>
</xsl:template>

<xsl:template match="constant">
  <xsl:call-template name="inline.monoseq"/>
</xsl:template>

<xsl:template match="database">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="errorcode">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="errorname">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="errortype">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="envar">
  <xsl:call-template name="inline.monoseq"/>
</xsl:template>

<xsl:template match="filename">
  <xsl:call-template name="inline.monoseq"/>
</xsl:template>

<xsl:template match="function">
  <xsl:choose>
    <xsl:when test="$function.parens != '0'
                    or parameter or function or replaceable"> 
      <xsl:variable name="nodes" select="text()|*"/>
      <xsl:call-template name="inline.monoseq">
        <xsl:with-param name="content">
          <xsl:apply-templates select="$nodes[1]"/>
        </xsl:with-param>
      </xsl:call-template>
      <xsl:text>(</xsl:text>
      <xsl:apply-templates select="$nodes[position()>1]"/>
      <xsl:text>)</xsl:text>
    </xsl:when>
    <xsl:otherwise>
     <xsl:call-template name="inline.monoseq"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="function/parameter" priority="2">
  <xsl:call-template name="inline.italicmonoseq"/>
  <xsl:if test="following-sibling::*">
    <xsl:text>, </xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="function/replaceable" priority="2">
  <xsl:call-template name="inline.italicmonoseq"/>
  <xsl:if test="following-sibling::*">
    <xsl:text>, </xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="replaceable" priority="1">
  <xsl:call-template name="inline.italicmonoseq"/>
</xsl:template>

<xsl:template match="guibutton">
  <xsl:call-template name="inline.boldcharseq"/>
</xsl:template>

<xsl:template match="guiicon">
  <xsl:call-template name="inline.boldcharseq"/>
</xsl:template>

<xsl:template match="guilabel">
  <xsl:call-template name="inline.boldcharseq"/>
</xsl:template>

<xsl:template match="guimenu">
  <xsl:call-template name="inline.boldcharseq"/>
</xsl:template>

<xsl:template match="guimenuitem">
  <xsl:call-template name="inline.boldcharseq"/>
</xsl:template>

<xsl:template match="guisubmenu">
  <xsl:call-template name="inline.boldcharseq"/>
</xsl:template>

<xsl:template match="hardware">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="interface">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="interfacedefinition">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="keycap">
  <xsl:call-template name="inline.boldseq"/>
</xsl:template>

<xsl:template match="keycode">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="keysym">
  <xsl:call-template name="inline.boldseq"/>
</xsl:template>

<xsl:template match="literal">
  <xsl:call-template name="inline.monoseq"/>
</xsl:template>

<xsl:template match="medialabel">
  <xsl:call-template name="inline.italicseq"/>
</xsl:template>

<xsl:template match="shortcut">
  <xsl:call-template name="inline.boldseq"/>
</xsl:template>

<xsl:template match="mousebutton">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="option">
  <xsl:call-template name="inline.monoseq"/>
</xsl:template>

<xsl:template match="parameter">
  <xsl:call-template name="inline.italicmonoseq"/>
</xsl:template>

<xsl:template match="property">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="prompt">
  <xsl:call-template name="inline.monoseq"/>
</xsl:template>

<xsl:template match="returnvalue">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="structfield">
  <xsl:call-template name="inline.italicmonoseq"/>
</xsl:template>

<xsl:template match="structname">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="symbol">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="systemitem">
  <xsl:call-template name="inline.monoseq"/>
</xsl:template>

<xsl:template match="token">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="type">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="userinput">
  <xsl:call-template name="inline.boldmonoseq"/>
</xsl:template>

<xsl:template match="abbrev">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="acronym">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="citerefentry">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="citetitle">
  <xsl:call-template name="inline.italicseq"/>
</xsl:template>

<xsl:template match="emphasis">
  <xsl:call-template name="inline.italicseq"/>
</xsl:template>

<xsl:template match="emphasis[@role='bold' or @role='strong']">
  <xsl:call-template name="inline.boldseq"/>
</xsl:template>

<xsl:template match="emphasis[@role='underline']">
  <xsl:call-template name="inline.underlineseq"/>
</xsl:template>

<xsl:template match="errortext">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="foreignphrase">
  <xsl:call-template name="inline.italicseq"/>
</xsl:template>

<xsl:template match="markup">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="phrase">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="quote">
  <xsl:call-template name="gentext.nestedstartquote"/>
  <xsl:call-template name="inline.charseq"/>
  <xsl:call-template name="gentext.nestedendquote"/>
</xsl:template>

<xsl:template match="varname">
  <xsl:call-template name="inline.monoseq"/>
</xsl:template>

<xsl:template match="wordasword">
  <xsl:call-template name="inline.italicseq"/>
</xsl:template>

<xsl:template match="lineannotation">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="superscript">
  <xsl:call-template name="inline.superscriptseq"/>
</xsl:template>

<xsl:template match="subscript">
  <xsl:call-template name="inline.subscriptseq"/>
</xsl:template>

<xsl:template match="trademark">
  <xsl:call-template name="inline.charseq"/>
  <xsl:call-template name="dingbat">
    <xsl:with-param name="dingbat">trademark</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template match="trademark[@class='copyright' or
                               @class='registered']">
  <xsl:call-template name="inline.charseq"/>
  <xsl:call-template name="dingbat">
    <xsl:with-param name="dingbat" select="@class"/>
  </xsl:call-template>
</xsl:template>

<xsl:template match="trademark[@class='service']">
  <xsl:call-template name="inline.charseq"/>
  <xsl:call-template name="inline.superscriptseq">
    <xsl:with-param name="content" select="'SM'"/>
  </xsl:call-template>
</xsl:template>

<xsl:template match="firstterm">
  <xsl:call-template name="inline.italicseq"/>
</xsl:template>

<xsl:template match="glossterm">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="keycombo">
  <xsl:variable name="action" select="@action"/>
  <xsl:variable name="joinchar">
    <xsl:choose>
      <xsl:when test="$action='seq'"><xsl:text> </xsl:text></xsl:when>
      <xsl:when test="$action='simul'">+</xsl:when>
      <xsl:when test="$action='press'">-</xsl:when>
      <xsl:when test="$action='click'">-</xsl:when>
      <xsl:when test="$action='double-click'">-</xsl:when>
      <xsl:when test="$action='other'"></xsl:when>
      <xsl:otherwise>-</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:for-each select="./*">
    <xsl:if test="position()>1"><xsl:value-of select="$joinchar"/></xsl:if>
    <xsl:text>\textbf{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
  </xsl:for-each>
</xsl:template>

<xsl:strip-space elements="menuchoice shortcut"/>

<xsl:template match="menuchoice">
  <xsl:variable name="shortcut" select="./shortcut"/>
  <!-- print the menuchoice tree -->
  <xsl:for-each select="*[not(self::shortcut)]">
    <xsl:if test="position() > 1">
      <xsl:choose>
        <xsl:when test="self::guimenuitem or self::guisubmenu">
          <xsl:text>\hspace{2pt}\ensuremath{\to{}}</xsl:text>
        </xsl:when>
        <xsl:otherwise>+</xsl:otherwise>
      </xsl:choose>
    </xsl:if>
    <xsl:text>\textbf{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
  </xsl:for-each>
  <!-- now, the shortcut if any -->
  <xsl:if test="$shortcut">
    <xsl:text> (</xsl:text>
    <xsl:apply-templates select="$shortcut"/>
    <xsl:text>)</xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="jobtitle|corpauthor|orgname|orgdiv">
  <xsl:apply-templates/>
</xsl:template>

<!-- ==================================================================== -->

<xsl:template match="optional">
  <xsl:value-of select="$arg.choice.opt.open.str"/>
  <xsl:call-template name="inline.charseq"/>
  <xsl:value-of select="$arg.choice.opt.close.str"/>
</xsl:template>

<!-- ==================================================================== -->

<xsl:template match="comment|remark">
  <xsl:if test="$show.comments != 0">
    <xsl:text>\marginpar{\footnotesize{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}}</xsl:text>
  </xsl:if>
</xsl:template>

<!-- ==================================================================== -->

<xsl:template match="productname">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<xsl:template match="productnumber">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

<!-- ==================================================================== -->

<xsl:template match="pob|street|city|state|postcode|country|phone|fax|otheraddr">
  <xsl:call-template name="inline.charseq"/>
</xsl:template>

</xsl:stylesheet>
