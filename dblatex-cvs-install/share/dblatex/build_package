#!/bin/sh

use_ssh=0
use_remote=`cat CVS/Root | grep -c "@"`
cvspackage=`cat CVS/Repository`
cvsroot=`cat CVS/Root`

# we can force some behaviour
while true
do
  case "$1" in
  --ssh) use_ssh=1 ;;
  --local) use_remote=0 ;;
  *) break ;;
  esac
  shift
done

if [ $use_ssh -eq 1 ]; then
  echo "use ssh mode"
  CVSROOT=:ext:marsgui@cvs.sourceforge.net:/cvsroot/dblatex
  export CVSROOT
  CVS_RSH=ssh
  export CVS_RSH
elif [ $use_remote -ne 0 ]; then
  CVSROOT=:pserver:anonymous@cvs.sourceforge.net:/cvsroot/dblatex
  export CVSROOT
else
  CVSROOT=$cvsroot
  export CVSROOT
fi

package=dblatex

if [ $# -lt 1 ]; then
  echo "$0 [--ssh] vX_Y_Z"
  exit 1
fi
tag=$1

# The actual release number is given from the tag:
#   vX_Y_Z -> X.Y.Z
#
version=`echo $tag|tr "_" "."|sed 's/^v//'`

release=$package-$version

echo "building version $version"

tmpdir=`mktemp -d /tmp/dblatex.XXXX`
if ! [ -d "$tmpdir" ]; then
  echo "Cannot create temporary directory $tmpdir"
  exit 1
fi

tmpbuild=$tmpdir/$release

cd $tmpdir; cvs -z3 -q export -r $tag -d $release $cvspackage; rc=$?
cd -

if [ $rc -ne 0 ]; then
  echo "CVS failed"
  exit 1
fi

# Remove the devel only files
rm -f $tmpbuild/build_package
rm -f $tmpbuild/xsl/common/.cvsignore
rm -f $tmpbuild/xsl/common/Makefile
rm -r $tmpbuild/tests/mathml/build
rm -f $tmpbuild/tests/mathml/excluded*

# Temporary manual cleanup
rm -rf $tmpbuild/xsl/latex-0.5.1
rm -rf $tmpbuild/latex/example

# Make the package version file
sed "s/devel/$version/" $tmpbuild/xsl/version.xsl > \
     $tmpbuild/xsl/version2.xsl
mv $tmpbuild/xsl/version2.xsl $tmpbuild/xsl/version.xsl

# Make the package doc
rm -f $tmpbuild/docs/*.pdf
gmake -C $tmpbuild/docs VERSION=$version

# Make the ball
tar cvf - -C $tmpdir $release | bzip2 > $release.tar.bz2
rm -r $tmpbuild

