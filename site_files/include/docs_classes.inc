<?php

class StylesheetGroup
{
	var $items = array();
	var $name;
	function StylesheetGroup($name,$group)
	{
		$this->name=$name;
		$this->items=explode(";",$group);
	}
	function show($alt)
	{
		if ( count($this->items) )
		{
			$numitems = count( $this->items );
			for ($i=0; $i < $numitems; $i++)
			{
				echo '<link rel="';
				if ($alt) echo "alternate stylesheet";
				else echo "stylesheet";
				echo '" media="screen" type="text/css" title="';
				echo $this->name;
				echo '" href="';
				echo $this->items[$i];
				echo "\" />\n";
			}
		}
	}
}

class StylesheetList
{
	var $items= array();
	function push($name,$group)
	{
		array_push($this->items,new StylesheetGroup($name,$group));
	}
	function show()
	{
		if ( count($this->items) )
		{
			$numitems = count( $this->items );
			for ($i=0; $i < $numitems; $i++)
			{
				$this->items[$i]->show($i>0);
			}
		}
	}
}



?>
