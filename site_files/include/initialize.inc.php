<?php
/*
	- Get the variables from the URL
	- Initialize to sane defaults if empty 
	- The differen levels of given parameters
	  are checked for existence.
	- Set to sane defaults if nonexistent
*/

function try_cleanup_input($input_string)
{
	/* keep only the first block of proper characters */
	$sane_pattern = "/^([[:alnum:]\-_\.\/]+)/";
	$input_matched = preg_match($sane_pattern, htmlentities($input_string), $matches);
	if ($input_matched) {
		return $matches[1];
	} else {
		return "";
	}
}

function get_variable($var_name, $default_value='')
{
	$var_value = "";
	if (!empty($_GET[$var_name])) {
		$var_value = try_cleanup_input($_GET[$var_name]);
	}
	if ($var_value === "") {
		$var_value = $default_value;
	}
	return $var_value;
}

/* Clear the nonexist indicators*/
$selected_branch_nonexist = "";
$selected_language_nonexist = "";
$selected_package_nonexist = "";
$selected_application_nonexist = "";

/* BRANCH: only valid if the other parameters have been specified,
   so an empty value is valid. If the values does not match
   an existing branch, the standard application page is shown
*/
$selected_branch = get_variable('branch');

/* branch for search */
$search_branch = get_variable('sbranch', 'trunk');


/* LANGUAGE */
$selected_language = get_variable('language', 'en');
if ( !in_array( $selected_language, $languagelist ) ) {
	/* Special rule for en_US */
	if ( $selected_language == "en_US" ) {
		$selected_language = "en";
	} else {
		/* Try to see if the language follows the xx_XX pattern, and
		   if yes, look for xx language code */
		$lang_pattern = "/([a-z]{2})_([A-Z]{2})/";
		$was_matched = preg_match($lang_pattern, $selected_language, $matches);
		if ( $was_matched and ( strtoupper($matches[1]) == $matches[2] ) and
		     in_array( $matches[1], $languagelist ) ) {
			$selected_language = $matches[1];
		} else {
			$selected_language_nonexist = $selected_language;
			$selected_language = "en";
		}
	}
}

/* PACKAGE */
$selected_package = get_variable('package');


/* APPLICATION */
// original input from user
$selected_application_from_user = get_variable('application');
// try the input, and if not found, try also the lowercase version
$selected_application = $selected_application_from_user;
$selected_application_found = false;
if ( array_key_exists( $selected_application_from_user, $programs_docs ) ) {
	$selected_application_found = true;
} else {
	$selected_application = strtolower($selected_application_from_user);
	if ( array_key_exists( $selected_application, $programs_docs ) ) {
		$selected_application_found = true;
	}
}
if ( ! $selected_application_found ) {
	$selected_application_nonexist = $selected_application;
	$selected_application = "";
}


/* PATH to html file under application (used to fix help: links) */
$html_path = get_variable('path', 'index.html');

?>
